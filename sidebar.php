<aside class="medium-4">
  <nav class="sidebar-nav sticky-sidebar">
    <ul class="menu">
      <li><a class="scroll-to" href="#typo">Typography</a></li>
      <li><a class="scroll-to" href="#tables">Tables</a></li>
      <li><a class="scroll-to" href="#medias">Medias</a></li>
      <li><a class="scroll-to" href="#buttons">Buttons</a></li>
      <li><a class="scroll-to" href="#forms">Forms</a></li>
      <li><a class="scroll-to" href="#menus">Menus</a></li>
      <li><a class="scroll-to" href="#grid">Grid</a></li>
      <li><a class="scroll-to" href="#visibility">Visibility</a></li>
      <li><a class="scroll-to" href="#spaces">Spaces</a></li>
    </ul>
  </nav>
</aside>
