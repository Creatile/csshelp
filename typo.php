<section class="section" id="typo">
  <h1> h1 Title</h1>
  <h2> h2 Title</h2>
  <h3> h3 Title</h3>
  <h4> h4 Title</h4>
  <h5> h5 Title</h5>
  <h6> h6 Title</h6>
  <p>Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960</p><p> grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.</p>
  <ul>
    <li>List element</li>
    <li>List element</li>
    <li>List element</li>
  </ul>
  <ol>
    <li>List element</li>
    <li>List element</li>
    <li>List element</li>
    <li>List element</li>
  </ol>
  <pre>
    <code>
      .test {
      background: #CCCCCC;
      }
    </code>
  </pre>
</section>