<section class="section" id="buttons">
  <p><span class="btn">Button</span></p>
  <p><span class="button">Button</span></p>
  <p><span class="btn-primary">Button primary</span></p>
  <p><span class="btn-secondary">Button secondary</span></p>
  <p><span class="btn-fluid">Button fluid</span></p>
  <p><span class="btn-large">Button large</span></p>
  <p><span class="btn-disabled">Button disabled</span></p>
  <p><button>Button </button></p>
</section>
