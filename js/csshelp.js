$(function(){
  /*SHOW NEXT ELEMENT*/
  $('.show-next').click(function(){
    $(this).next().toggleClass("show");
  });

  /*HIDE NEXT ELEMENT*/
  $('.hide-next').click(function(){
    $(this).next().toggleClass("hide");
  });

  /*HIDE MESSAGE*/
  $('.message').prepend('<span class="close">&times</span>');
  $('.message .close').click(function(){
    $(this).parent().addClass("hide");
  });

  /*SHOW  MENU*/
  $('.hamburger').click(function(){
    $(this).parent().toggleClass("display-menu");
  });

  /*FIX WHEN REACH TOP*/

  $("#scrolled").on("scroll", function() {
    var pos = $(this).scrollTop();
    console.log(pos);
    if (pos == 0) {
      alert('top of the div');
    }

  });

});
