<section class="section" id="grid">
  <h2>Grid</h2>
  <div class="container">
    <div class="row">
      <div class="small-6">50%</div>
      <div class="small-6">50%</div>
      <div class="small-8">8 col</div>
      <div class="small-4">4 col</div>
    </div>
  </div>
  <div class="container medium">
    <div class="row">
      <div class="medium-6">50%</div>
      <div class="medium-6">50%</div>
      <div class="medium-8">8 col</div>
      <div class="medium-4">4 col</div>
    </div>
  </div>

  <div class="container fluid">
    <div class="row">
      <div class="large-6">50%</div>
      <div class="large-6">50%</div>
      <div class="large-8">8 col</div>
      <div class="large-4">4 col</div>
    </div>
  </div>
</section>
