<section class="section" id="forms">
  <h2>forms</h2>
  <form class="form-example" method="post">
    <fieldset>
      <legend>Personal Information</legend>

      <label for="full-name">
        Full Name
        <span class="required">
          (required)
        </span>

        <span class="error-message">
            You must input a real name.
        </span>

        <input id="full-name" name="full-name" required="" type="text">

      </label>

      <label for="email">
        Email
        <span class="required">
          (required)
        </span>

        <span class="error-message">
          The email address you entered is not valid.
        </span>

        <input id="email" name="email" required="" type="email">

      </label>

    </fieldset>

    <fieldset>
      <legend>Credit Card Information</legend>

      <label for="cc">
        Credit Card Number
        <span class="required">
          (required)
        </span>

        <span class="error-message">
            Your credit card number should be all numbers.
        </span>

        <input id="cc" name="cc" required="" type="text" minlength='13' maxlength='20' pattern='\s*[0-9]+-?\s*[0-9]+-?\s*[0-9]+-?\s*[0-9]+-?\s*'>

      </label>
    </fieldset>

    <fieldset>
      <label for='text-comments'>
        Please leave your comments or questions here
        <span class="required">
          (required)
        </span>

        <span class="error-message">
          Please give us a little more information about your inquiry.
        </span>

        <textarea id='text-comments' required=''></textarea>
      </label>
    </fieldset>

    <fieldset>
      <legend id='legend-1'>Can we add you to our email mailing list? <span class="required">(required)</span></legend>

      <ul aria-labelledby='legend-1' role='radiogroup'>
        <li>
          <label for="yes">
            <span class="error-message">
              Please select yes or no.
            </span>

            <input id="yes" required="" type="radio" name="answers" value="yes">

            Yes
          </label>
        </li>
        <li>
          <label for="no">
            <input id="no" type="radio" name="answers" value="no">

            No
          </label>
        </li>
      </ul>
    </fieldset>

    <fieldset>
      <legend id='legend-2'>Which topics interest you? <span class="required">(required)</span></legend>

      <ul aria-labelledby='legend-2' role='group'>
        <li>
          <label for="science">
            <span class="error-message">
              Please select at least one category.
            </span>

            <input id="science" required="" type="checkbox" name="categories" value="science">

            Science
          </label>
        </li>
        <li>
          <label for="children-programs">
            <input id="children-programs" required="" type="checkbox" name="categories" value="children-programs">

            Programs for Children
          </label>
        </li>
        <li>
          <label for="new-events">
            <input id="new-events" required="" type="checkbox" name="categories" value="new-events">

            New Events
          </label>
        </li>
      </ul>
    </fieldset>

    <fieldset>
      <label for="options">Choose from the following:
        <span class="required">
          (required)
        </span>

        <span class="error-message">
          Please choose one of the following options.
        </span>
        <select name="options" id="options" required=''>
          <option value=''> Select </option>
          <option value="value1">Option A</option>
          <option value="value2">Option B</option>
          <option value="value3">Option C</option>
        </select>
      </label>
    </fieldset>

    <button class='form-example-submit' type="submit">Submit Form</button>
  </form>

  <form method="post">
    <h1>Payment form (reset)</h1>
    <p>
      <strong>Required fields</strong> are followed by
      <strong><abbr title="required">*</abbr></strong
      >.
    </p>
    <section>
      <h2>Contact information</h2>
      <fieldset>
        <legend>Title</legend>
        <ul>
          <li>
            <label for="title_1">
              <input type="radio" id="title_1" name="title" value="A" />
              Ace
            </label>
          </li>
          <li>
            <label for="title_2">
              <input type="radio" id="title_2" name="title" value="K" />
              King
            </label>
          </li>
          <li>
            <label for="title_3">
              <input type="radio" id="title_3" name="title" value="Q" />
              Queen
            </label>
          </li>
        </ul>
      </fieldset>
      <p>
        <label for="name">
          <span>Name: </span>
          <strong><abbr title="required">*</abbr></strong>
        </label>
        <input type="text" id="name" name="username" />
      </p>
      <p>
        <label for="mail">
          <span>E-mail: </span>
          <strong><abbr title="required">*</abbr></strong>
        </label>
        <input type="email" id="mail" name="usermail" />
      </p>
      <p>
        <label for="pwd">
          <span>Password: </span>
          <strong><abbr title="required">*</abbr></strong>
        </label>
        <input type="password" id="pwd" name="password" />
      </p>
    </section>
    <section>
      <h2>Payment information</h2>
      <p>
        <label for="card">
          <span>Card type:</span>
        </label>
        <select id="card" name="usercard">
          <option value="visa">Visa</option>
          <option value="mc">Mastercard</option>
          <option value="amex">American Express</option>
        </select>
      </p>
      <p>
        <label for="number">
          <span>Card number:</span>
          <strong><abbr title="required">*</abbr></strong>
        </label>
        <input type="tel" id="number" name="cardnumber" />
      </p>
      <p>
        <label for="date">
          <span>Expiration date:</span>
          <strong><abbr title="required">*</abbr></strong>
          <em>formatted as mm/dd/yyyy</em>
        </label>
        <input type="date" id="date" name="expiration" />
      </p>
    </section>
    <section>
      <h2>Rates</h2>
      <table>
        <tr>
          <th>Base</th>
          <th>Rate</th>
        </tr>
        <tr>
          <td>0</td>
          <td>0.1</td>
        </tr>
        <tr>
          <td>1</td>
          <td>0.2</td>
        </tr>
        <tr>
          <td>2</td>
          <td>0.4</td>
        </tr>
        <tr>
          <td>3</td>
          <td>0.7</td>
        </tr>
      </table>
    </section>
    <section>
      <p><button type="submit">Validate the payment</button></p>
    </section>
  </form>
</section>